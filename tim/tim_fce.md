# Čítače / Časovače - Knihovní funkce

V této kapiotle je souhrn nejpoužívanějších funkcí pro práci s timery.

## Předpoklady

Tato kapitola předpokládá základy jazyka C.

## Funkce společné pro všechny časovače

Následující funkce jsou společné jak pro `basic` tak i `general purpose` timery. Funkce začínají `TIMx_JmenoFunkce`, `x` se nahradí číslem timeru, který chceme ovládat.

### Inicializace časovače

Funkce nastaví předděličku a strop čítače (hodnotu do které čítač čítá). Důležité je si dávat pozor, že každý čítač má své vlastní předděličky.

```c
void TIM4_TimeBaseInit(
    TIM4_Prescaler_TypeDef TIM4_Prescaler, // předdělička
    uint8_t TIM4_Period                    // strop čítače (ARR)
);
```

<details>
<summary>Předděličky</summary>

Ukázky předděliček `TIM4`:

```c
typedef enum
{
  TIM4_PRESCALER_1   = ((uint8_t)0x00),
  TIM4_PRESCALER_2   = ((uint8_t)0x01),
  TIM4_PRESCALER_4   = ((uint8_t)0x02),
  TIM4_PRESCALER_8   = ((uint8_t)0x03),
  TIM4_PRESCALER_16  = ((uint8_t)0x04),
  TIM4_PRESCALER_32  = ((uint8_t)0x05),
  TIM4_PRESCALER_64  = ((uint8_t)0x06),
  TIM4_PRESCALER_128 = ((uint8_t)0x07)
} TIM4_Prescaler_TypeDef;
```

Ukázky předděliček `TIM2`:

```c
typedef enum {
    TIM2_PRESCALER_1     = ((uint8_t) 0x00),
    TIM2_PRESCALER_2     = ((uint8_t) 0x01),
    TIM2_PRESCALER_4     = ((uint8_t) 0x02),
    TIM2_PRESCALER_8     = ((uint8_t) 0x03),
    TIM2_PRESCALER_16    = ((uint8_t) 0x04),
    TIM2_PRESCALER_32    = ((uint8_t) 0x05),
    TIM2_PRESCALER_64    = ((uint8_t) 0x06),
    TIM2_PRESCALER_128   = ((uint8_t) 0x07),
    TIM2_PRESCALER_256   = ((uint8_t) 0x08),
    TIM2_PRESCALER_512   = ((uint8_t) 0x09),
    TIM2_PRESCALER_1024  = ((uint8_t) 0x0A),
    TIM2_PRESCALER_2048  = ((uint8_t) 0x0B),
    TIM2_PRESCALER_4096  = ((uint8_t) 0x0C),
    TIM2_PRESCALER_8192  = ((uint8_t) 0x0D),
    TIM2_PRESCALER_16384 = ((uint8_t) 0x0E),
    TIM2_PRESCALER_32768 = ((uint8_t) 0x0F)
} TIM2_Prescaler_TypeDef;
```

✍ _`Advance` timery jako například `TIM1` nemají hodnotu předděličky zakódovanou, ale můžemu ji přiřadit libovolné číslo._

</details>

### Spuštění / zastavení časovače

```c
void TIM4_Cmd(FunctionalState NewState);
```

<details>
<summary>Ukázka použití</summary>

```c
TIM4_Cmd(ENABLE);  // spuštění čítače
TIM4_Cmd(DISABLE); // zastavení čítače
```

</details>

### Čtení vlajky čítače

```c
FlagStatus TIM4_GetFlagStatus(TIM4_FLAG_TypeDef TIM4_FLAG);
```

<details>
<summary>Ukázka použití</summary>

```c
// čekání dokud se nenastaví vlajka přetečení
while (TIM4_GetFlagStatus(TIM4_FLAG_UPDATE) != SET)
    ;
```

</details>

### Nulování vlajky čítače

```c
void TIM4_ClearFlag(TIM4_FLAG_TypeDef TIM4_FLAG);
```

<details>
<summary>Ukázka použití</summary>

```c
// vynulování vlajky přetečení
TIM4_ClearFlag(TIM4_FLAG_UPDATE);
```

</details>

### Nastavení hodnoty čítače

```c
void TIM4_SetCounter(uint8_t Counter);
```

<details>
<summary>Ukázka použití</summary>

```c
// vynulování hodnoty čítače
TIM4_SetCounter(0);
```

</details>

### Přečtení hodnoty čítače

```c
uint8_t TIM4_GetCounter(void);
```

<details>
<summary>Ukázka použití</summary>

```c
// uložení hodnoty čítače do proměnné
uint8_t tim4_cnt_value = TIM4_GetCounter();
```

</details>

### Nastavení stropu čítače

```c
void TIM4_SetAutoreload(uint8_t Autoreload);
```

<details>
<summary>Ukázka použití</summary>

```c
// nastavení stropu čítače na hodnotu 100
TIM4_SetAutoreload(100);
```

</details>


### Ovládání preload hodnoty stropu

Pokud je aktivní nová hodnota stropu se uplatní až po přetečení čítače.

```c
void TIM4_ARRPreloadConfig(FunctionalState NewState);
```

<details>
<summary>Ukázka použití</summary>

```c
TIM4_ARRPreloadConfig(ENABLE);  // povolí přednadčítání stropu
TIM4_ARRPreloadConfig(DISABLE); // hodnota stropu bude ihned uplatněna
```

</details>

## Práce s kanály časovačů

S kanály je to podobné jako s časovači je jich více a každý má svou inicializační funkci. Například funkce `TIM2_OC2Init` inicializuje 2. kanál `TIM2`, kdežto `TIM2_OC3Init` inicializuje 3. kanál téhož timeru.

### Inicializace kanálu

```c
void TIM2_OC2Init(
    TIM2_OCMode_TypeDef TIM2_OCMode,           // funkce kanálu (mód)
    TIM2_OutputState_TypeDef TIM2_OutputState, // HW výstup
    uint16_t TIM2_Pulse,                       // hodnota capture / compare registru
    TIM2_OCPolarity_TypeDef TIM2_OCPolarity    // možnost negace výstupu
);
```

<details>
<summary>Ukázka použití</summary>

```c
TIM2_OC2Init(                  // inicializace kanálu OC2
    TIM2_OCMODE_PWM1,          // funkce kanálu PWM1
    TIM2_OUTPUTSTATE_ENABLE,   // povolit HW výstup
    4000,                      // překlápěcí práh
    TIM2_OCPOLARITY_HIGH       // polarita výsledného signálu
);
```

</details>

### Preload hodnoty Capture / Compare registru

```c
void TIM2_OC3PreloadConfig(FunctionalState NewState);
```

<details>
<summary>Ukázka použití</summary>

```c
TIM2_OC3PreloadConfig(ENABLE);  // povolí přednadčítání prahu kanálu 3
TIM2_OC3PreloadConfig(DISABLE); // hodnota prahu bude ihned uplatněna
```

</details>

### Nastavení hodnoty prahu

```c
void TIM2_SetCompare1(uint16_t Compare1);
```

<details>
<summary>Ukázka použití</summary>

```c
// nastavení prahu TIM2 kanálu 1 na hodnotu 42
TIM2_SetCompare1(42);
```

</details>

## Přerušení časovače

Funkce pro nastavení a řízení přerušení.

### Inicializace přerušení

```c
void TIM2_ITConfig(
    TIM2_IT_TypeDef TIM2_IT, // zdroj přerušení
    FunctionalState NewState // povolení / zakázání zdroje přerušení
);
```

<details>
<summary>Ukázka použití</summary>

```c
// povolení přerušení při přetečení
TIM2_ITConfig(TIM2_IT_UPDATE, ENABLE);
```

</details>


### Nastavení priority přerušení

```c
void ITC_SetSoftwarePriority(
    ITC_Irq_TypeDef IrqNum,                 // zdroj přerušení
    ITC_PriorityLevel_TypeDef PriorityValue // priorita
);
```

<details>
<summary>Ukázka použití</summary>

```c
// nastavení priority přerušení přetečení na 1
ITC_SetSoftwarePriority(ITC_IRQ_TIM2_OVF, ITC_PRIORITYLEVEL_1);
```

</details>

### Globální povolení přerušení

```c
#define enableInterrupts()  __asm__("rim")
```

<details>
<summary>Ukázka použití</summary>

```c
// globální povolení přerušení
enableInterrupts();
```

</details>

### Globální zakázání přerušení

```c
#define disableInterrupts() __asm__("sim")
```

<details>
<summary>Ukázka použití</summary>

```c
// globální zakázání přerušení
disableInterrupts();
```

</details>
